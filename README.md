## Requirements:
- Python >= 3.7
- pip3


## Installation:
1. `git clone https://gitlab.com/AlexKird/csv_splitter.git`
2. `cd csv_splitter`
3. `pip3 install -r requirements`


## How to use:
1. Move target csv files(for example: file1.csv file2.csv) into folder csv_splitter
2. `python3 main.py file1.csv file2.csv -p 10 --zip` 
3. Result will be in files *file1.csv-result.zip* and *file2.csv-result.zip*

## Examples:

1. Split one file into 10 parts without zip:
`python3 main.py file1.csv -p 10`

2. With zip:
`python3 main.py file1.csv -p 10 --zip`

3. Split three(or more) files into 10 parts each and zip:
`python3 main.py file1.csv file2.csv file3.csv -p 10 --zip`
