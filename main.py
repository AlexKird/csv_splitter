import pandas as pd
from zipfile import ZipFile, ZIP_DEFLATED
import os
import argparse
import shutil


def split_csv(filename: str, parts_count: int) -> str:
    directory = str(f'{filename}-result')
    print(f'✂ start splitting {filename}')
    num_of_lines = sum(1 for line in open(filename))
    print(f'🧮 total lines in file: {num_of_lines}')
    os.mkdir(directory)
    for i, chunk in enumerate(pd.read_csv(
            filename, chunksize=int(num_of_lines / parts_count),
            low_memory=True)):
        new_filename = f'{directory}/{i + 1}-{filename}'
        print(f'✏ writing: {new_filename}({i + 1}/{parts_count})')
        chunk.to_csv(new_filename, index=False)
    return directory


def zip_path(path: str) -> str:
    print(f'📦 zip {path}')
    with ZipFile(f'{path}.zip', 'w', ZIP_DEFLATED) as zf:
        for dirname, subdirs, files in os.walk("mydirectory"):
            zf.write(dirname)
            for filename in files:
                zf.write(os.path.join(dirname, filename))
    shutil.rmtree(path, ignore_errors=True)
    return f'{path}.zip'


parser = argparse.ArgumentParser()
parser.add_argument('filenames', metavar='N', nargs='+',
                    help='Name of files to split. '
                         'Example: file1.csv file2.csv file3.csv', type=str)
parser.add_argument('-p', '--parts', help='Count of parts. Example: -p 10',
                    type=int, required=True)
parser.add_argument('-z', '--zip',
                    help='pass --zip i put result in zip archive', action='count')
args = parser.parse_args()

try:
    for i, file in enumerate(args.filenames):
        if len(args.filenames) > 1:
            print(f'{i}/{len(args.filenames)} processing file {file}')
        path = split_csv(file, args.parts)
        if args.zip is not None:
            path = zip_path(path)

        print(f'🔥 DONE! result in: {path}')
except Exception as e:
    print('😦 Ohh noo! something went wrong!')
input('Press any key to exit..')